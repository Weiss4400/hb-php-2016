<?php

namespace AppBundle\Controller;

use AppBundle\Entity\FilmService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * @Route("/films")
 */
class FilmController extends Controller
{
    /**
     * @var FilmService
     */
    private $filmService;

    /**
     * FilmController constructor.
     */
    public function __construct()
    {
        $this->filmService = new FilmService();
    }


    /**
     * @Route("/list")
     */
    public function listAction()
    {

        return $this->render('film/list.html.twig', array(
            // ...
        ));
    }

    /**
     * @Route("/detail")
     */
    public function detailAction()
    {
        return $this->render('film/detail.html.twig', array(
            // ...
        ));
    }

}
